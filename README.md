With this module you can multiply two positive integers using the [russian peasant](https://en.wikipedia.org/wiki/Ancient_Egyptian_multiplication#Russian_peasant_multiplication) algorithm.

#Installation
     
     npm install russian-peasant


#Usage

     russian = require('russian-peasant')
     twenty = russian.multiply(5,4)
     console.log(twenty) // 20
    
The two parameters *a* and *b* must be positive integers. They must not be bigger than 2^31-1 or 2 147 483 647.
