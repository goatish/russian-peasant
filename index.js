"use strict";

/**
 * @param a {number}
 * @param b {number}
 * @returns {number}
 */
multiply = function (a, b) {
    var result = 0;
    // check whether params can be processed by russian
    check(a, b);
    // as long as a is bigger than zero
    while (a > 0) {
        // if a is not even
        if (a % 2 === 1) {
            // add b to the result
            result += b;
        }
        // halve a
        a = a >> 1;
        // double b
        b = b << 1;
    }
    if(a!== 0 && b!==0 && result === 0) {
        throw "Result is out of bound"
    }
    return result;
};

/**
 * Check whether params meet criteria
 * @param a {number}
 * @param b {number}
 */
check = function (a, b) {
    var upper_bound = Math.pow(2, 31) - 1;
    if (a < 0 || b < 0) {
        throw "russian only works with *positive* integers."
    }
    if (a % 2 !== 1 && a % 2 !== 0 || b % 2 !== 1 && b % 2 !== 0) {
        throw "russian only works with whole numbers. (integer)"
    }
    if (a > upper_bound || b > upper_bound) {
        throw "russian only works with numbers smaller than 2^31-1"
    }
};

module.exports = {multiply:multiply};