/**
 * Created by user on 13.11.16.
 */
"use strict";

var russian = require("./index");
var assert = require("chai").assert;

for(var i = 0; i<10; i++){
    var a = Math.floor(Math.random() * 1000),
        b = Math.floor(Math.random() * 1000);

    assert.equal(russian.multiply(a, b), a * b);
    //console.log(a.toString() + " * " + b.toString() + " = " + (a*b).toString());
}

console.log("All tests passed successfully.");